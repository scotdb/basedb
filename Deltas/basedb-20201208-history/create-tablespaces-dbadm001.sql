CREATE LARGE TABLESPACE TS_DBADM001_8K
PAGESIZE  8 K
MANAGED BY AUTOMATIC STORAGE
BUFFERPOOL BP8K_TAB_STANDARD;

CREATE LARGE TABLESPACE TS_DBADM001_32K
PAGESIZE 32 K
MANAGED BY AUTOMATIC STORAGE
BUFFERPOOL BP32K_TAB_STANDARD;
