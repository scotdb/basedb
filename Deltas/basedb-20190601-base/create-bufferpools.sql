create bufferpool bp8k_tab_standard  size 1000 automatic pagesize 8k;
create bufferpool bp32k_tab_standard size 1000 automatic pagesize 32k;
create bufferpool bp32k_temp         size 1000 automatic pagesize 32k;
