--WHENEVER SQLERROR EXIT
SPOOL /tmp/test.log
SET ESCAPE OFF
STA @create-bufferpools.sql
STA @create-systemp-ts.sql
STA @create-usertemp-ts.sql
STA @CROSSLOAD.spsql
STA @RESET_IDENTITY.spsql
STA @CONNECT_PROC.spsql
STA @grant-connect_proc.sql
UPDATE DB CFG USING CONNECT_PROC DBADMIN.CONNECT_PROC;
SPOOL OFF
QUIT

