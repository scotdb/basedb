#!/usr/bin/ksh93

#
# build-base.sh
# =============
# Builds a base database shell with standard objects
#
# Parameters
# ----------
# 1 : Database name
# 2 : Instance port number
# 3 : Instance owner password
# 4 : DAta directory suffix
# 5 : Number of data directories
# 6 : Use SSL for configuration (Y/N)
#
# Notes
# -----
# Assumes database name in format Desssnnn (e.g. DTABC001).
# Will derive default schema DBsss001 from this and create this schema.
#
# (C) ScotDB Limited 
#

# Check that $DB2_HOME is set
if [ -z "$DB2_HOME" ]
then
  echo "Run db2profile to set up DB2 environment"; exit 8;
else
  echo "DB2 environment set correctly"
fi

# Check whether running as DB2 instance owner
if [ "$HOME/sqllib" != "$DB2_HOME" ]; then
  echo "Must run as DB2 instance owner"; exit 8;
else
  echo "Running as instance owner : $USER";
fi

. $DB2_HOME/db2profile

#
# Check for inputs
#
if [ $# = 6 ]; then
  echo "Six arguments passed : treated as dbname, port number and password";
  dbname="$1";
  portnum="$2";
  pword="$3";
  datadir="$4";
  datanum="$5";
  usessl="$6";
else
  #
  # If no inputs then prompt
  #
  echo "Unexpected number of arguments passed : prompting for data";
  print -n "Enter database name (lower case): "; read dbname;
  print -n "Enter DB2 instance port number: "; read portnum;
  # Do not echo password to screen
  stty -echo;print -n "Enter DB2 instance password: "; read pword;stty echo;printf "\n";
  print -n "Enter data directory prefix: "; read datadir;
  print -n "Enter number of data directories: "; read datanum;
  print -n "Use SSL connection for build: "; read usessl;
fi

# Check operating system
os=`uname`;
echo "Running on $os";

# Make database name lower alphabetic case
dbname=$(echo $dbname|tr "[:upper:]" "[:lower:]");
echo $dbname;

if echo $dbname | egrep -q '^d(a|b|d|p|t).{3}[0-9]{3}$'
then
    echo "DB Name Good"
else
    echo "Bad database name : standard is D(A|B|D|P|T)<sss><nnn>"; exit 8;
fi

# Build schema name from database name
tlq=${dbname:2:3};
echo $tlq;
schema="DB"${dbname:2:3}"001";
schema=$(echo $schema|tr "[:lower:]" "[:upper:]");
echo $schema;

if echo $portnum | egrep -q '^[0-9]{5}$'
then
  echo "Port number good";
else
  echo "Bad port number"; exit 8;
fi

#
# Validate the $usessl parameter
#
usessl=$(echo $usessl|tr "[:lower:]" "[:upper:]");
if echo $usessl | egrep -q '^(Y|N)$'
then
  echo "Using SSL/TLS: "$usessl;
else
  echo "Bad parameter for SSL usage : should by Y or N";
  exit 8;
fi  

#
# Now extract the number of the instance
# (assumes instances are called db2inst<n>)
#
dbinstnum=${USER:7:1};
echo $dbinstnum;

#
# Check for instance specific data directories (e.g. /db2data11) v /db2data01
#
testdir="/"$datadir$dbinstnum"1";
echo $testdir;
if [[ -d ${testdir} ]]; then
  echo "Instance specific directories found";
else
  dbinstnum="0";
fi

#
# Now check existance of base directories (based on instance)
#
i=0
for ((i=1;i<=$datanum;i++))
do
  dir="/"$datadir$dbinstnum$i;
  if [ -d $dir ] 
  then
    echo "$dir exists";
  else
    echo "$dir does not exist"; exit 8;
  fi
done

for i in 1 2
do
  dir="/db2log"$dbinstnum$i;
  # echo $dir;
  if [ -d $dir ] 
  then
    echo "$dir exists";
  else
    echo "$dir does not exist"; exit 8;
  fi
done

echo "[INFO] $(date) : makedbdirs.sh $dbname $dbinstnum $datadir $datanum"
ksh93 makedbdirs.sh $dbname $dbinstnum $datadir $datanum;             
echo "[INFO] $(date) : create-database.sh $dbname $dbinstnum $datadir $datanum"
ksh93 create-database.sh $dbname $dbinstnum $datadir $datanum;
echo "[INFO] $(date) : update-db-cfg.sh $dbname $dbinstnum"
ksh93 update-db-cfg.sh $dbname $dbinstnum;

#
# Take backup (AIX to TSM, Linux to local directory
#
bucmd="backup database $dbname ";
if [[ "$os" = "AIX" ]]; then
  bucmd="$bucmd USE TSM COMPRESS";
else
  bucmd=$bucmd" to /db2bck"$dbinstnum"1/"$dbname" compress"
fi
echo "[INFO] $(date) : $bucmd";
db2 "$bucmd"

echo "[INFO] $(date) : grantsecadm.sh $dbname db2sadm1";
ksh93 grantsecadm.sh $dbname db2sadm1;

echo "[INFO] $(date) : revokepublic.sh $dbname";
ksh93 revokepublic.sh $dbname;

#
# Now prepare to use CLPPlus over SSL
#
export DB2DSDRIVER_CFG_PATH=`pwd`;


if [[ "usessl" = "Y" ]]; then
  #
  # Now prepare to use CLPPlus over SSL
  #  
  #echo `db2 get dbm cfg|grep SSL_SVCENAME`;
  cp $HOME/security/keys_cms.arm db2server.arm
  cp db2dsdriver.cfg.template.tls db2dsdriver.cfg
else
  cp db2dsdriver.cfg.template.notls db2dsdriver.cfg
fi  

hostname=`hostname`;

if [[ "$os" = "AIX" ]]; then
  sed 's/$DBALIAS/'$dbname'/g' db2dsdriver.cfg >db2dsdriver.cfg.temp && mv db2dsdriver.cfg.temp db2dsdriver.cfg
  sed 's/$DBNAME/'$dbname'/g' db2dsdriver.cfg >db2dsdriver.cfg.temp && mv db2dsdriver.cfg.temp db2dsdriver.cfg
  sed 's/$DBHOST/'$hostname'/g' db2dsdriver.cfg >db2dsdriver.cfg.temp && mv db2dsdriver.cfg.temp db2dsdriver.cfg
  sed 's/$DBPORT/'$portnum'/g' db2dsdriver.cfg >db2dsdriver.cfg.temp && mv db2dsdriver.cfg.temp db2dsdriver.cfg
  sed 's/$USERID/'$USER'/g' db2dsdriver.cfg >db2dsdriver.cfg.temp && mv db2dsdriver.cfg.temp db2dsdriver.cfg
  sed 's/$PASSWORD/'$pword'/g' db2dsdriver.cfg >db2dsdriver.cfg.temp && mv db2dsdriver.cfg.temp db2dsdriver.cfg
else
  sed -i 's/$DBALIAS/'$dbname'/g' db2dsdriver.cfg
  sed -i 's/$DBNAME/'$dbname'/g' db2dsdriver.cfg
  sed -i 's/$DBHOST/'$hostname'/g' db2dsdriver.cfg
  sed -i 's/$DBPORT/'$portnum'/g' db2dsdriver.cfg
  sed -i 's/$USERID/'$USER'/g' db2dsdriver.cfg
  sed -i 's/$PASSWORD/'$pword'/g' db2dsdriver.cfg
fi  

clpplus -nw $USER@$dbname @basedb.clp

rm db2dsdriver.cfg

if [[ "$usessl" = "Y" ]]; then
  rm db2server.arm
fi  

echo "[INFO] $(date) : create-schema.sh $dbname $schema $dbinstnum";
ksh93 create-schema.sh $dbname $schema $dbinstnum;
