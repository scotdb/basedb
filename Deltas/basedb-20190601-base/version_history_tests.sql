--<ScriptOptions statementTerminator="#"/>

--TESTING
--create variable
create variable DBADMIN.dbs_test varchar(128)#

--get_current_version should return NULL
set DBADMIN.dbs_test = DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN')#
select DBADMIN.dbs_test from sysibm.dual#

--deploy v 2.00
call DBADMIN.MD_SCHEMA_VERSION.signal_start_deployment(p_in_schema => 'DBADMIN',p_in_current_version => DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN'),p_in_new_version => '2.00',p_in_new_version_comments => 'Rel Notes v2.00')#
call DBADMIN.MD_SCHEMA_VERSION.signal_end_deployment(p_in_schema => 'DBADMIN', p_in_version => DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN',p_in_only_ongoing_deployments => 'Y'))#
select * from DBADMIN.T0030VERSION_HISTORY#

--deploy v 2.01
call DBADMIN.MD_SCHEMA_VERSION.signal_start_deployment(p_in_schema => 'DBADMIN',p_in_current_version => DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN'),p_in_new_version => '2.01',p_in_new_version_comments => 'Rel Notes v2.01')#
call DBADMIN.MD_SCHEMA_VERSION.signal_end_deployment(p_in_schema => 'DBADMIN', p_in_version => DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN',p_in_only_ongoing_deployments => 'Y'))#
select * from DBADMIN.T0030VERSION_HISTORY#

--backout version 2.01
call DBADMIN.MD_SCHEMA_VERSION.signal_start_backout(p_in_schema => 'DBADMIN', p_in_version => DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN'))#
call DBADMIN.MD_SCHEMA_VERSION.signal_end_backout(p_in_schema => 'DBADMIN', p_in_version => DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN'))#
select * from DBADMIN.T0030VERSION_HISTORY#

--redeploy version 2.01
call DBADMIN.MD_SCHEMA_VERSION.signal_start_deployment(p_in_schema => 'DBADMIN',p_in_current_version => '2.00',p_in_new_version => '2.01',p_in_new_version_comments => 'Rel Notes v2.01')#
call DBADMIN.MD_SCHEMA_VERSION.signal_end_deployment(p_in_schema => 'DBADMIN', p_in_version => DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN',p_in_only_ongoing_deployments => 'Y'))#
select * from DBADMIN.T0030VERSION_HISTORY#

--delete v2.01 by schema / version no
call DBADMIN.MD_SCHEMA_VERSION.delete_version(p_in_schema => 'DBADMIN',p_in_version=>'2.01',p_out_rowcount => DBADMIN.dbs_test)#
select DBADMIN.dbs_test from sysibm.dual#
select * from DBADMIN.T0030VERSION_HISTORY#

--redeploy version 2.01
call DBADMIN.MD_SCHEMA_VERSION.signal_start_deployment(p_in_schema => 'DBADMIN',p_in_current_version => '2.00',p_in_new_version => '2.01',p_in_new_version_comments => 'Rel Notes v2.01')#
call DBADMIN.MD_SCHEMA_VERSION.signal_end_deployment(p_in_schema => 'DBADMIN', p_in_version => DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN',p_in_only_ongoing_deployments => 'Y'))#
select * from DBADMIN.T0030VERSION_HISTORY#

--delete v2.01 by ID
call DBADMIN.MD_SCHEMA_VERSION.delete_version(p_in_ID => 4,p_out_rowcount => DBADMIN.dbs_test)#
select DBADMIN.dbs_test from sysibm.dual#
select * from DBADMIN.T0030VERSION_HISTORY#

--redeploy version 2.01
call DBADMIN.MD_SCHEMA_VERSION.signal_start_deployment(p_in_schema => 'DBADMIN',p_in_current_version => '2.00',p_in_new_version => '2.01',p_in_new_version_comments => 'Rel Notes v2.01')#
call DBADMIN.MD_SCHEMA_VERSION.signal_end_deployment(p_in_schema => 'DBADMIN', p_in_version => DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => 'DBADMIN',p_in_only_ongoing_deployments => 'Y'))#
select * from DBADMIN.T0030VERSION_HISTORY#

--EXPECTED FAIL - repeat signal_start_deployment call should cause error as 2.01 already deployed
call DBADMIN.MD_SCHEMA_VERSION.signal_start_deployment(p_in_schema => 'DBADMIN',p_in_current_version => '2.01',p_in_new_version => '2.01',p_in_new_version_comments => 'Rel Notes v2.01')#
--EXPECTED FAIL - repeat signal_end_deployment call should cause error as 2.01 already deployed
call DBADMIN.MD_SCHEMA_VERSION.signal_end_deployment(p_in_schema => 'DBADMIN', p_in_version => '2.01')#
--EXPECTED FAIL - call signal_start_deployment with incorrect current version should cause error
call DBADMIN.MD_SCHEMA_VERSION.signal_start_deployment(p_in_schema => 'DBADMIN',p_in_current_version => '2.00',p_in_new_version => '2.01',p_in_new_version_comments => 'Rel Notes v2.01')#
--EXPECTED FAIL - call signal_start_deployment with incorrect (null) current version should cause error
call DBADMIN.MD_SCHEMA_VERSION.signal_start_deployment(p_in_schema => 'DBADMIN',p_in_current_version => null,p_in_new_version => '2.01',p_in_new_version_comments => 'Rel Notes v2.01')#

drop variable DBADMIN.dbs_test#