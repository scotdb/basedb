#!/usr/bin/ksh93
#
# Parameters
# ==========
# $dbname : Database name (lower case)
# $instnum : Instance number (single digit)
# $3 : data directory stub
# $4 : number of data directories
#
# (C) ScotDB Limited
#

dbname="$1";
instnum="$2";
basedir="$3";
numdir="$4";

os=`uname`;
echo "Running on $os";
if [[ "$os" = "AIX" ]]; then
  echo "Backups will be configured to go to TSM";
else
  mkdir -p "/db2bck"$instnum"1/"$dbname
fi

for ((i=1;i<=$numdir;i++))
do	
  mkdir -p "/"$basedir$instnum$i"/"$dbname
done  
mkdir -p "/db2log"$instnum"1/active/"$dbname
mkdir -p "/db2log"$instnum"1/archive/"$dbname
mkdir -p "/db2log"$instnum"1/failure/"$dbname
mkdir -p "/db2log"$instnum"1/overflow/"$dbname
mkdir -p "/db2log"$instnum"1/temp/"$dbname
mkdir -p "/db2log"$instnum"2/active/"$dbname
mkdir -p "/db2log"$instnum"2/archive/"$dbname
