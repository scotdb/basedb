#!/usr/bin/ksh93
#
# Parameters
# ==========
# $1 : database name (lower case)
# $2 : instance number
#
# (C) ScotDB Limited
#

. $DB2_HOME/db2profile

os=`uname`;
echo "Running on $os";

db2 "UPDATE DB CFG FOR $1 USING LOCKTIMEOUT     30 "
db2 "UPDATE DB CFG FOR $1 USING NEWLOGPATH      /db2log$21/active/$1 "
db2 "UPDATE DB CFG FOR $1 USING OVERFLOWLOGPATH /db2log$21/overflow/$1 "
db2 "UPDATE DB CFG FOR $1 USING FAILARCHPATH    /db2log$21/failure/$1 "

if [[ "$os" = "AIX" ]]; then
  db2 "UPDATE DB CFG FOR $1 USING LOGARCHMETH1    TSM "
else
  db2 "UPDATE DB CFG FOR $1 USING LOGARCHMETH1    DISK:/db2log$22/archive/$1 "
fi

db2 "UPDATE DB CFG FOR $1 USING LOGFILSIZ       4000 "
db2 "UPDATE DB CFG FOR $1 USING LOGPRIMARY      25 "
db2 "UPDATE DB CFG FOR $1 USING LOGSECOND       150"
db2 "UPDATE DB CFG FOR $1 USING BLOCKNONLOGGED  YES "
db2 "UPDATE DB CFG FOR $1 USING NUM_DB_BACKUPS  7 "
db2 "UPDATE DB CFG FOR $1 USING REC_HIS_RETENTN 7 "
db2 "UPDATE DB CFG FOR $1 USING AUTO_DEL_REC_OBJ ON "
