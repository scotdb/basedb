--<ScriptOptions statementTerminator=";">

--Requirements:
--versions are managed at schema level
--target_schema, version, and backout_end_ts in combination must be unique (allows same version to be deployed multiple times if backed out)
--get the current version by running sp check_current_version

--Improvements ; To do
--Version is a single varachar column at present, if a version naming standard can be agreed e.g. majver.minver.pointreleasenumber, we could split the field into parts and apply logic to ensure new version nos always exceed the current deployed version (i.e. no installing old version by accident)   
--Currently in DBADMIN - Move into Admin schema and grant nec privs
--create views (optionally specific to a schema(s)) and grant privs to these to allow priv allocation at schema level 
--more testing
--tidy up

-- Table DBADMIN.VERSION_HISTORY
-- Function MD_SCHEMA_VERSION.check_current_version (i.e. highest non backed out version)
-- Procedure MD_SCHEMA_VERSION.signal_start_deployment 
-- Procedure MD_SCHEMA_VERSION.signal_end_deployment 
-- Procedure MD_SCHEMA_VERSION.signal_start_backout
-- Procedure MD_SCHEMA_VERSION.signal_end_backout
-- Procedure MD_SCHEMA_VERSION.delete_version (overloaded) 
-- Procedure MD_SCHEMA_VERSION.delete_version (overloaded)

--deployment process--
--1. call signal_start_deployment
--2. call deployment script
--3. call signal_end_deployment

--backout process
--1. call signal_start_backout
--2. call backout script
--3. call signal_end_deployment

drop module DBADMIN.MD_SCHEMA_VERSION;
drop public ALIAS SCHEMA_VERSION;
drop table DBADMIN.VERSION_HISTORY;
drop tablespace scph003;

create tablespace SCPH003
pagesize 32k
managed by automatic storage
bufferpool BP32K_TAB_STANDARD;

CREATE TABLE DBADMIN.VERSION_HISTORY     
(ID                       BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (
                          START WITH +1
                           INCREMENT BY +1
                           MINVALUE +1
                           NO MAXVALUE
                           NO CYCLE
                           NOCACHE
                           NO ORDER ),
 TARGET_SCHEMA             VARCHAR(128) NOT NULL,
 VERSION                   VARCHAR(128) NOT NULL, --varchar to allow nn.nn.nn.nn convention
 VERSION_COMMENTS          VARCHAR(32000),
 DEPLOY_BY_SESSION_USER    VARCHAR(128) NOT NULL DEFAULT SESSION_USER,
 DEPLOY_BY_USER            VARCHAR(128) NOT NULL DEFAULT USER,
 DEPLOY_BY_CURRENT_SQLID   VARCHAR(128) NOT NULL DEFAULT CURRENT SQLID, 
 DEPLOY_START_TS           TIMESTAMP(6) NOT NULL DEFAULT CURRENT TIMESTAMP,
 DEPLOY_END_TS             TIMESTAMP(6),
 BACKOUT_BY_SESSION_USER   VARCHAR(128),
 BACKOUT_BY_USER           VARCHAR(128),
 BACKOUT_BY_CURRENT_SQLID  VARCHAR(128),
 BACKOUT_START_TS          TIMESTAMP(6),
 BACKOUT_END_TS            TIMESTAMP(6),
 CONSTRAINT Pcph003        PRIMARY KEY (ID)
) IN SCPH003
COMPRESS YES;

CALL SYSPROC.ADMIN_CMD('RUNSTATS ON TABLE DBADMIN.VERSION_HISTORY');

--PROMPT DBADMIN.VERSION_HISTORY (TARGET_SCHEMA, VERSION)
--PROMPT NOTE - prevents same version being deployed (unless it is already backed out)
   CREATE UNIQUE INDEX DBADMIN.XCPH003A0 ON DBADMIN.VERSION_HISTORY (TARGET_SCHEMA, VERSION, BACKOUT_END_TS)
   	MINPCTUSED 0
	ALLOW REVERSE SCANS
	PAGE SPLIT SYMMETRIC
	COLLECT DETAILED STATISTICS
	COMPRESS YES;

--CREATE PUBLIC SYNONYM FOR REQUIRED TABLE
CREATE PUBLIC ALIAS SCHEMA_VERSION FOR DBADMIN.VERSION_HISTORY;

--MODULE MD_SCHEMA_VERSION
CREATE MODULE DBADMIN.MD_SCHEMA_VERSION;

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION PUBLISH FUNCTION get_current_version
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA, IN p_in_only_ongoing_deployments varchar(1) DEFAULT ('N'))
returns ANCHOR SCHEMA_VERSION.VERSION
LANGUAGE SQL;

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION ADD FUNCTION get_current_version
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA, IN p_in_only_ongoing_deployments varchar(1) DEFAULT ('N'))
returns ANCHOR SCHEMA_VERSION.VERSION
BEGIN
  declare c_no_schema_specified condition for sqlstate '99001';
  declare l_version ANCHOR SCHEMA_VERSION.VERSION;
  if p_in_schema is null then
    signal c_no_schema_specified set message_text = 'Please specify a value for p_in_schema.';
  end if;
  select VERSION into l_version
  from SCHEMA_VERSION 
  where TARGET_SCHEMA = p_in_schema
  --optionally filter for ongoing deployments
  and (
       (DEPLOY_END_TS is null and nvl(p_in_only_ongoing_deployments,'N') = 'Y')
        or
        nvl(p_in_only_ongoing_deployments,'N') != 'Y'
      )
  and BACKOUT_END_TS is null  
  order by DEPLOY_START_TS DESC
  FETCH FIRST 1 ROW ONLY;
  return l_version;
END/

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION PUBLISH PROCEDURE signal_start_deployment 
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_current_version ANCHOR SCHEMA_VERSION.VERSION,IN p_in_new_version ANCHOR SCHEMA_VERSION.VERSION,IN p_in_new_version_comments ANCHOR SCHEMA_VERSION.VERSION_COMMENTS)
LANGUAGE SQL;

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION ADD PROCEDURE signal_start_deployment
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_current_version ANCHOR SCHEMA_VERSION.VERSION,IN p_in_new_version ANCHOR SCHEMA_VERSION.VERSION,IN p_in_new_version_comments ANCHOR SCHEMA_VERSION.VERSION_COMMENTS)
BEGIN
  declare c_parameter_not_specified condition for sqlstate '99001';
  declare c_not_correct_version condition for sqlstate '99002';
  declare l_error_message varchar(500);
  declare l_version ANCHOR SCHEMA_VERSION.VERSION;
  if p_in_schema is null or p_in_new_version is null then
    signal c_parameter_not_specified set message_text = 'Please specify a value for both p_in_schema and p_in_new_version.';
  end if;
  set l_version = DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => p_in_schema);
  if nvl(l_version,'NuLl') != nvl(p_in_current_version,'NuLl') then
    set l_error_message = 'Actual current version = "' || nvl(l_version,'NULL') || '", p_in_current_version = "' || nvl(p_in_current_version,'NULL') || '".';
    signal c_not_correct_version set message_text = l_error_message;
  end if;
  insert into SCHEMA_VERSION (TARGET_SCHEMA,VERSION, VERSION_COMMENTS) values (p_in_schema,p_in_new_version,p_in_new_version_comments);
  commit;
END/

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION PUBLISH PROCEDURE signal_end_deployment 
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_version ANCHOR SCHEMA_VERSION.VERSION)
LANGUAGE SQL;

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION ADD PROCEDURE signal_end_deployment
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_version ANCHOR SCHEMA_VERSION.VERSION)
BEGIN
  declare c_parameter_not_specified condition for sqlstate '99001';
  declare c_not_correct_version condition for sqlstate '99002';
  declare l_error_message varchar(500);
  declare l_version ANCHOR SCHEMA_VERSION.VERSION;
  declare l_ID ANCHOR SCHEMA_VERSION.ID;
  if p_in_schema is null or p_in_version is null then
    signal c_parameter_not_specified set message_text = 'Please specify a value for both p_in_schema and p_in_version.';
  end if;
  set l_version = DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => p_in_schema, p_in_only_ongoing_deployments => 'Y');
  if l_version is null then
    signal c_not_correct_version set message_text = 'No ongoing deployment.';
  end if;
  if nvl(l_version,'NuLl') != p_in_version then
    set l_error_message = 'Ongoing deployment = "' || nvl(l_version,'NULL') || '", p_in_version = "' || nvl(p_in_version,'NULL') || '".';
    signal c_not_correct_version set message_text = l_error_message;
  end if;
  select ID
  into l_ID
  from final table
	(
  		update SCHEMA_VERSION
  		set DEPLOY_END_TS = CURRENT TIMESTAMP
  		where TARGET_SCHEMA = p_in_schema
  		and version = p_in_version
  		and DEPLOY_START_TS is not null
  		and DEPLOY_END_TS is null
  		and BACKOUT_START_TS is null
  		and BACKOUT_END_TS is null
  	);
  if l_ID is null then
    set l_error_message = 'Version "' || nvl(p_in_version,'NULL') || '" is already deployed, or backed out for ' || p_in_schema || '.';
      signal c_not_correct_version set message_text = l_error_message;
    end if;
  commit;
END/

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION PUBLISH PROCEDURE signal_start_backout 
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_version ANCHOR SCHEMA_VERSION.VERSION)
LANGUAGE SQL;

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION ADD PROCEDURE signal_start_backout
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_version ANCHOR SCHEMA_VERSION.VERSION)
BEGIN
  declare c_parameter_not_specified condition for sqlstate '99001';
  declare c_not_correct_version condition for sqlstate '99002';
  declare l_error_message varchar(500);
  declare l_version ANCHOR SCHEMA_VERSION.VERSION;
  declare l_ID ANCHOR SCHEMA_VERSION.ID;
  if p_in_schema is null or p_in_version is null then
    signal c_parameter_not_specified set message_text = 'Please specify a value for both p_in_schema and p_in_version.';
  end if;
  set l_version = DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => p_in_schema);
  if l_version != p_in_version then
    set l_error_message = 'Actual current version = "' || nvl(l_version,'NULL') || '", p_in_version = "' || nvl(p_in_version,'NULL') || '".';
    signal c_not_correct_version set message_text = l_error_message;
  end if;
  select ID
  into l_ID
  from final table
	(
  		update SCHEMA_VERSION
  		set BACKOUT_START_TS          = CURRENT TIMESTAMP
  		   ,BACKOUT_BY_SESSION_USER   = SESSION_USER
           ,BACKOUT_BY_USER           = USER
           ,BACKOUT_BY_CURRENT_SQLID  = CURRENT SQLID
  		where TARGET_SCHEMA = p_in_schema
  		and version = p_in_version
  		and DEPLOY_START_TS is not null
  		and DEPLOY_END_TS is not null
  		and BACKOUT_START_TS is null
  		and BACKOUT_END_TS is null
  	);
  if l_ID is null then
    set l_error_message = 'Version has not completed deployment or is already started backing out.';
      signal c_not_correct_version set message_text = l_error_message;
    end if;
  commit;
END/

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION PUBLISH PROCEDURE signal_end_backout 
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_version ANCHOR SCHEMA_VERSION.VERSION)
LANGUAGE SQL;

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION ADD PROCEDURE signal_end_backout
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_version ANCHOR SCHEMA_VERSION.VERSION)
BEGIN
  declare c_parameter_not_specified condition for sqlstate '99001';
  declare c_not_correct_version condition for sqlstate '99002';
  declare l_error_message varchar(500);
  declare l_version ANCHOR SCHEMA_VERSION.VERSION;
  declare l_ID ANCHOR SCHEMA_VERSION.ID;
  if p_in_schema is null or p_in_version is null then
    signal c_parameter_not_specified set message_text = 'Please specify a value for both p_in_schema and p_in_version.';
  end if;
  set l_version = DBADMIN.MD_SCHEMA_VERSION.get_current_version(p_in_schema => p_in_schema);
  if l_version != p_in_version then
    set l_error_message = 'Actual current version = "' || nvl(l_version,'NULL') || '", p_in_version = "' || nvl(p_in_version,'NULL') || '".';
    signal c_not_correct_version set message_text = l_error_message;
  end if;
  select ID
  into l_ID
  from final table
	(
  		update SCHEMA_VERSION
  		set BACKOUT_END_TS          = CURRENT TIMESTAMP
  		where TARGET_SCHEMA = p_in_schema
  		and version = p_in_version
  		and DEPLOY_START_TS is not null
  		and DEPLOY_END_TS is not null
  		and BACKOUT_START_TS is not null
  		and BACKOUT_END_TS is null
  	);
  if l_ID is null then
    set l_error_message = 'Version backout has not started.';
      signal c_not_correct_version set message_text = l_error_message;
    end if;
  commit;
END/

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION PUBLISH PROCEDURE delete_version 
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_version ANCHOR SCHEMA_VERSION.VERSION, out p_out_rowcount integer)
LANGUAGE SQL;

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION ADD PROCEDURE delete_version
(IN p_in_schema ANCHOR SCHEMA_VERSION.TARGET_SCHEMA,IN p_in_version ANCHOR SCHEMA_VERSION.VERSION, out p_out_rowcount integer)
BEGIN
  declare c_parameter_not_specified condition for sqlstate '99001';
  if p_in_schema is null or p_in_version is null then
    signal c_parameter_not_specified set message_text = 'Please specify a value for both p_in_schema and p_in_version.';
  end if;
  select count(*)
  into p_out_rowcount
  from old table
	(
  		delete from SCHEMA_VERSION
  		where TARGET_SCHEMA = p_in_schema
  		and VERSION = p_in_version
  	);
  commit;
END/

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION PUBLISH PROCEDURE delete_version 
(IN p_in_id ANCHOR SCHEMA_VERSION.ID, out p_out_rowcount integer)
LANGUAGE SQL;

ALTER MODULE DBADMIN.MD_SCHEMA_VERSION ADD PROCEDURE delete_version
(IN p_in_id ANCHOR SCHEMA_VERSION.ID, out p_out_rowcount integer)
BEGIN
  declare c_parameter_not_specified condition for sqlstate '99001';
  if p_in_id is null then
    signal c_parameter_not_specified set message_text = 'Please specify a value for p_in_id.';
  end if;
  select count(*)
  into p_out_rowcount
  from old table
	(
  		delete from SCHEMA_VERSION
  		where ID = p_in_ID
  	);
  commit;
END/