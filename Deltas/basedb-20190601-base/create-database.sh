#!/usr/bin/ksh93
#
# Parameters
# ==========
# $1 : database name (lower case)
# $2 : instance number
# $3 : base for data directories
# $4 : number of data directories
#
# (C) ScotDB Limited
#
. $DB2_HOME/db2profile

dbname="$1";
instnum="$2";
basedir="$3";
numdir="$4";

createcmd="CREATE DATABASE "$dbname" AUTOMATIC STORAGE YES ON ";
for ((i=1;i<=$numdir;i++))
do
  if [[ $i -ne 1 ]]; then
    createcmd=$createcmd",";
  fi
  createcmd=$createcmd"/"$basedir$instnum$i"/"$dbname;  
done	
createcmd=$createcmd" DBPATH ON ";
createcmd=$createcmd"/"$basedir$instnum"1/"$dbname;
createcmd=$createcmd" USING CODESET UTF-8 TERRITORY US";
createcmd=$createcmd" ENCRYPT";
createcmd=$createcmd" CATALOG   TABLESPACE MANAGED BY AUTOMATIC STORAGE";
createcmd=$createcmd" USER      TABLESPACE MANAGED BY AUTOMATIC STORAGE";
createcmd=$createcmd" TEMPORARY TABLESPACE MANAGED BY AUTOMATIC STORAGE";

echo $createcmd;

db2 $createcmd;
