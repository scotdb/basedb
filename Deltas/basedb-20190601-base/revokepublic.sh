#!/usr/bin/ksh93
#
# Parameters
# ==========
# $1 : Database name (lower case)

db2 "connect to $1"
db2 "revoke connect on database from public"
db2 "connect reset"
