#!/usr/bin/ksh93
#
# Parameters
# ==========
# $1 : Database name (lower case)
# $2 : SECADM group name
#
# (C) ScotDB Limited
#

db2 "connect to $1"
db2 "grant secadm on database to group $2"
db2 "connect reset"
