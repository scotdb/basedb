#!/usr/bin/ksh93
#
# Parameters
# ==========
# $1 : Database name (lower case)
# $2 : Instance number (single digit)

echo "******* BE CAREFUL *******"
echo -n "Y to Continue: "
read cont

#echo "|"$cont"|"

if [[ $cont =~ ^[Yy]$ ]]
then
    echo "I will continue"
else
	echo "I will Stop"
	exit
fi    


rm -r /db2data$21/$1
rm -r /db2data$22/$1
rm -r /db2data$23/$1
rm -r /db2data$24/$1
rm -r /db2log$21/active/$1
rm -r /db2log$21/archive/$1
rm -r /db2log$21/failure/$1
rm -r /db2log$21/overflow/$1
rm -r /db2log$21/temp/$1
rm -r /db2log$22/active/$1
rm -r /db2log$22/archive/$1
rm -r /db2bck$21/$1
