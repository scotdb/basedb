#!/usr/bin/ksh93
#
# Parameters
# ==========
# $1 : Database name (lower case)
# $2 : schema name
# $3 : instance number
# $4 : owning auth ID (session auth)
#
# Modified to all secondary schemas to be produced
# with more flexible names
#
# (C) ScotDB Limited
#

db2 "connect to $1"

# create schema

db2 "create schema $2 authorization $4"

# Create specific tablespace for schema

# ts8k="TS_DB"${2:2:3}"001_8K"
ts8k="TS_"${2}"_8K"
# ts32k="TS_DB"${2:2:3}"001_32K"
ts32k="TS_"${2}"_32K"

db2 "CREATE LARGE TABLESPACE $ts8k
PAGESIZE  8 K
MANAGED BY AUTOMATIC STORAGE
BUFFERPOOL BP8K_TAB_STANDARD"

db2 "CREATE LARGE TABLESPACE $ts32k
PAGESIZE 32 K
MANAGED BY AUTOMATIC STORAGE
BUFFERPOOL BP32K_TAB_STANDARD"

db2 "grant use of tablespace $ts8k to user $4"
db2 "grant use of tablespace $ts32k to user $4"
# db2 "grant setsessionuser on user $4 to group db2dev"$3"1"
# db2 "grant connect on database to group db2dev"$3"1"
# dbdevgrp=${4:2:3}"dev"$3"1"
# dbdevgrp=$(echo $dbdevgrp|tr "[:lower:]" "[:upper:]");
# db2 "grant setsessionuser on user $4 to group $dbdevgrp"
db2 "grant connect on database to group $dbdevgrp" 
db2 "grant load on database to user $2"
db2 "grant accessctrl on schema $2 to user $4"
 
db2 "connect reset"

echo "Please request the following groups to be created (if they do not already exist) -"
echo "db2dev"$3"1"
echo $dbdevgrp
