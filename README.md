# BaseDB

This repository contains scripts for creating a basic Db2 database.

It includes configuring session authorisation and setting up some useful utilities

## Prerequisites

- Db2 installed
- Local execution on Db2 server
- Native encryption configured
- Base data directories created
- Log directories /db2log11 and /db2log21 with active/archive/overflow/failure/temp subdirectories

## Setting Up Encryption

- gsk8capicmd_64 -keydb -create -db "/home/db2inst1/security/native.p12" -pw "<password>" -strong -type pkcs12 -stash
- db2 "update dbm cfg using keystore_location /home/db2inst1/security/native.p12 keystore_type pkcs12"

